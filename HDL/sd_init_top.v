module sd_init_top(
    input   wire        i_clk_50M,
    input   wire        i_rst_n,
    input   wire        i_miso,
    
    output  wire        o_mosi,
    output  wire        o_cs_n,
    output  wire        o_init_end
);

    wire        clk_25M;
    wire        clk_25M_shift;
    wire        sys_rst_n;

    clk_gen U_clk_gen(
        .i_clk_50M      (i_clk_50M),
        .i_rst_n        (i_rst_n),

        .o_clk_25M      (clk_25M),
        .o_clk_25M_shift(clk_25M_shift),
        .o_rst_n        (sys_rst_n)
    );

    sd_init U_sd_init(
        .i_sys_clk      (clk_25M),                      //系统时钟
        .i_sys_clk_shift(clk_25M_shift),                //产生相移的系统时钟
        .i_rst_n        (sys_rst_n),                    //复位，低有效
        .i_miso         (i_miso),                       //SD卡回传信号

        .o_mosi         (o_mosi),                       //主机发送指令/数据
        .o_cs_n         (o_cs_n),                       //片选，低有效
        .o_init_end     (o_init_end)                    //SD卡初始化完成
    );  

endmodule