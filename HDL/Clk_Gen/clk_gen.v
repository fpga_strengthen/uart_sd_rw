module clk_gen(
    input   wire        i_clk_50M,
    input   wire        i_rst_n,

    output  wire        o_clk_25M,
    output  wire        o_clk_25M_shift,
    output  wire        o_rst_n
);

    wire    locked;

    pll U_pll(
	    .areset (~i_rst_n),
	    .inclk0 (i_clk_50M),
	    .c0     (o_clk_25M),                //相位无偏移
	    .c1     (o_clk_25M_shift),          //相位偏移45°
	    .locked (locked)
    );

    assign  o_rst_n = i_rst_n & locked;

endmodule