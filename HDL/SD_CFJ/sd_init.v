module sd_init(
    input   wire        i_sys_clk,                  //系统时钟
    input   wire        i_sys_clk_shift,            //产生相移的系统时钟
    input   wire        i_rst_n,                    //复位，低有效
    input   wire        i_miso,                     //SD卡回传信号

    output  reg         o_mosi,                     //主机发送指令/数据
    output  reg         o_cs_n,                     //片选，低有效
    output  wire        o_init_end                  //SD卡初始化完成
);  

    //控制指令
    parameter   CMD0   = {8'h40,32'h00_00_00_00,8'h95},
                CMD8   = {8'h48,32'h00_00_01_AA,8'h87},
                CMD55  = {8'h77,32'h00_00_00_00,8'hFF},
                ACMD41 = {8'h69,32'h40_00_00_00,8'hFF};

    //状态声明，共10个状态，独热码表示
    parameter   INIT_IDLE   = 4'b0000,
                SEND_CMD0   = 4'b0001,
                CMD0_ACK    = 4'b0011,
                SEND_CMD8   = 4'b0111,
                CMD8_ACK    = 4'b0110,
                SEND_CMD55  = 4'b1110,
                CMD55_ACK   = 4'b1111,
                SEND_ACMD41 = 4'b1011,
                ACMD41_ACK  = 4'b1001,
                INIT_END    = 4'b1000;

    parameter   CNT_WAIT_MAX = 7'd100;


    //上电后的等待时间，至少74个clk
    reg     [6:0]   cnt_wait;

    //等待时间足够时保持
    always@(posedge i_sys_clk or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            cnt_wait <= 7'd0;
        else if(cnt_wait == CNT_WAIT_MAX)
            cnt_wait <= cnt_wait;
        else
            cnt_wait <= cnt_wait + 1'b1;
    end

    //状态变量
    reg     [3:0]   state;

    always@(posedge i_sys_clk or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            state <= INIT_IDLE;
        else begin 
            case(state)
                INIT_IDLE : begin 
                    if(cnt_wait == CNT_WAIT_MAX - 1'b1)
                        state <= SEND_CMD0;
                    else
                        state <= state;
                end
                SEND_CMD0 : begin 
                    if(cnt_cmd_bit == 6'd48)
                        state <= CMD0_ACK;
                    else
                        state <= state;
                end
                CMD0_ACK : begin
                    if(cnt_ack_bit == 6'd47)begin 
                        if(ack_data[39:32] == 8'h01)
                            state <= SEND_CMD8;
                        else
                            state <= SEND_CMD0;
                    end
                    else
                        state <= state;
                end
                SEND_CMD8 : begin
                    if(cnt_cmd_bit == 6'd48)
                        state <= CMD8_ACK;
                    else
                        state <= state;
                end
                CMD8_ACK : begin
                    if(cnt_ack_bit == 6'd47)begin 
                        if(ack_data[39:32] == 8'h01)
                            state <= SEND_CMD55;
                        else
                            state <= SEND_CMD8;
                    end
                    else
                        state <= state;
                end
                SEND_CMD55 : begin
                    if(cnt_cmd_bit == 6'd48)
                        state <= CMD55_ACK;
                    else
                        state <= state;
                end
                CMD55_ACK : begin
                    if(cnt_ack_bit == 6'd47)begin 
                        if(ack_data[39:32] == 8'h01)
                            state <= SEND_ACMD41;
                        else
                            state <= SEND_CMD55;
                    end
                    else
                        state <= state;
                end
                SEND_ACMD41 : begin 
                    if(cnt_cmd_bit == 6'd48)
                        state <= ACMD41_ACK;
                    else
                        state <= state;
                end
                ACMD41_ACK : begin
                    if(cnt_ack_bit == 6'd47)begin 
                        if(ack_data[39:32] == 8'h00)
                            state <= INIT_END;
                        else
                            state <= SEND_ACMD41;
                    end
                    else
                        state <= state;
                end
                INIT_END : state <= state;
            endcase
        end
    end

    reg     [5:0]   cnt_cmd_bit;
    reg     [5:0]   cnt_ack_bit;

    //对指令做计数，计数值0~39为前5个字节的相应，40~47为片选信号拉高前的等待，48时拉高
    always@(posedge i_sys_clk or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            cnt_cmd_bit <= 6'd0;
        else if(cnt_cmd_bit == 6'd48)
            cnt_cmd_bit <= 6'd0;
        else if((state == SEND_CMD0)||(state == SEND_CMD8)||(state == SEND_CMD55)||(state == SEND_ACMD41))
            cnt_cmd_bit <= cnt_cmd_bit + 1'b1;
        else
            cnt_cmd_bit <= cnt_cmd_bit;
    end

    //发送MOSI指令
    always@(posedge i_sys_clk or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            o_mosi <= 1'b1;             //空闲状态时，发送高电平
        else begin 
            case(state)
                INIT_IDLE : o_mosi <= 1'b1;
                SEND_CMD0 : begin 
                    if((cnt_cmd_bit >= 6'd1) && (cnt_cmd_bit <= 6'd48))
                        o_mosi <= CMD0[48 - cnt_cmd_bit];       //从MSB开始发送
                    else
                        o_mosi <= o_mosi;
                end
                SEND_CMD8 : begin 
                    if((cnt_cmd_bit >= 6'd1) && (cnt_cmd_bit <= 6'd48))
                        o_mosi <= CMD8[48 - cnt_cmd_bit];
                    else
                        o_mosi <= o_mosi;              
                end
                SEND_CMD55 : begin 
                    if((cnt_cmd_bit >= 6'd1) && (cnt_cmd_bit <= 6'd48))
                        o_mosi <= CMD55[48 - cnt_cmd_bit];
                    else
                        o_mosi <= o_mosi;              
                end
                SEND_ACMD41 : begin 
                    if((cnt_cmd_bit >= 6'd1) && (cnt_cmd_bit <= 6'd48))
                        o_mosi <= ACMD41[48 - cnt_cmd_bit];
                    else
                        o_mosi <= o_mosi;              
                end
                default : o_mosi <= 1'b1;
            endcase
        end
    end

    //对输入的miso打拍提取上升沿
    reg     miso_reg;
    wire    miso_n;

    always@(posedge i_sys_clk_shift or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            miso_reg <= 1'b1;
        else
            miso_reg <= i_miso;
    end

    assign  miso_n = (i_miso == 1'b0)&&(miso_reg == 1'b1);

    //ack_en作为对回传数据计数的使能信号
    reg             ack_en;

    //该使能信号是开始收到回传的miso有效信号的标志
    always@(posedge i_sys_clk_shift or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            ack_en <= 1'b0;
        else if(miso_n == 1'b1)
            ack_en <= 1'b1;
        else if(cnt_ack_bit == 6'd47)
            ack_en <= 1'b0;
        else 
            ack_en <= ack_en;
    end

    //对miso接收的比特计数
    always@(posedge i_sys_clk_shift or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            cnt_ack_bit <= 6'd0;
        else if(cnt_ack_bit == 6'd48)
            cnt_ack_bit <= 6'd0;
        else if(ack_en == 1'b1)
            cnt_ack_bit <= cnt_ack_bit + 1'b1;
        else
            cnt_ack_bit <= 6'd0;
    end

    //将miso接收的比特寄存
    reg     [39:0]  ack_data;

    //对打拍后的信号寄存，会更稳定
    always@(posedge i_sys_clk_shift or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            ack_data <= 40'b0;
        else if((ack_en == 1'b1)&&(cnt_ack_bit <= 6'd39))
            ack_data <= {ack_data[38:0],miso_reg};      //miso接收的比特是从MSB开始的
        else
            ack_data <= ack_data;
    end

    //片选信号
    always@(posedge i_sys_clk or negedge i_rst_n) begin
        if(i_rst_n == 1'b0)
            o_cs_n <= 1'b1;
        else begin 
            case(state)
                INIT_IDLE : o_cs_n <= 1'b1;
                SEND_CMD0,SEND_CMD8,SEND_CMD55,SEND_ACMD41 : begin 
                    if(cnt_cmd_bit == 6'd47)
                        o_cs_n <= 1'b1;
                    else
                        o_cs_n <= 1'b0;
                end
                CMD0_ACK,CMD8_ACK,CMD55_ACK,ACMD41_ACK : begin 
                    if(cnt_ack_bit == 6'd48)
                        o_cs_n <= 1'b1;
                    else
                        o_cs_n <= 1'b0;
                end
                default : o_cs_n <= 1'b1;
            endcase
        end
    end

    assign      o_init_end = (state == INIT_END);

endmodule